﻿using System;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class Coverage
    {
        public float FromAngle;
        public float ToAngle;
    }
}
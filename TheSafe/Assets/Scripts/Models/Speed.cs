﻿namespace Assets.Scripts.Models
{
    public enum Speed
    {
        None        = 0,
        SuperSlow   = 50,
        Slow        = 100,
        Medium      = 200,
        Fast        = 400,
        SuperFast   = 800,
        InHuman     = 1200
    }
}
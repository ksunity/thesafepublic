﻿namespace Assets.Scripts.Models
{
    public enum PatternType
    {
        Simple,
        FromTo,
        Jump
    }
}
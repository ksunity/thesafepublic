﻿namespace Assets.Scripts.Models
{
    public class CodePattern
    {
        public PatternType Type { get; set; }
        public int Digit { get; set; }
        public Range Range { get; set; }
        public Speed Speed { get; set; }
        public bool MoveToLeft { get; set; }
        public float StartPosition { get; set; }
        public float[] PositionParams { get; set; }

        public CodePattern(PatternType type, int digit, Range range, Speed speed, bool moveToLeft, float startPosition, params float[] positionParams)
        {
            Type = type;
            Digit = digit;
            Range = range;
            Speed = speed;
            MoveToLeft = moveToLeft;
            StartPosition = startPosition;
            PositionParams = positionParams;
        }
    }
}

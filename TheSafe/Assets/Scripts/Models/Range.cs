﻿namespace Assets.Scripts.Models
{
    public enum Range
    {
        Tiny        = 9,
        Small       = 18,
        Normal      = 36,
        Wide        = 72,
        VeryWide    = 108,
    }
}
﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class WonUnlockedController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private TextMeshProUGUI _unlockedText;

        private bool _start;
        private bool _done;
        private float _currentAlpha;

        public void FadeIn()
        {
            _start = true;
            _done = false;
            _text.enabled = true;
            _unlockedText.text = "Unlocked";
            _unlockedText.color = Color.green;
        }

        public bool IsDone()
        {
            return _done;
        }

        private void Start()
        {
            ResetAll();
        }

        public void ResetAll()
        {
            _currentAlpha = 0f;
            _text.enabled = false;
            SetTextAlpha(_currentAlpha);
            _start = false;
            _done = false;
            _unlockedText.text = "Locked";
            _unlockedText.color = Color.red;
        }
        
        private void SetTextAlpha(float value)
        {
            var titleColor = _text.color;
            var newTitleColor = new Color(titleColor.r, titleColor.g, titleColor.b, value);
            _text.color = newTitleColor;
        }

        private void Update()
        {
            if(!_start) return;
            _currentAlpha += 0.25f * Time.deltaTime;
            if (_currentAlpha >= 1f) _currentAlpha = 1f;
            SetTextAlpha(_currentAlpha);
            if (_currentAlpha >= 1f)
            {
                _start = false;
                _done = true;
            }
        }
    }
}

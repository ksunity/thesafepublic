﻿using System;
using UnityEngine.UI;

namespace Assets.Scripts.Controller
{
    [Serializable]
    public class StateIndicator
    {
        public bool Unlocked = false;
        public bool Unlocking = false;
        public Image ClosedStrip;
        public Image ClosedCircle;
        public Image OpenStrip;
        public Image OpenCircle;

        public void Reset()
        {
            Unlocked = false;
            Unlocking = false;

            OpenStrip.enabled = false;
            OpenCircle.enabled = false;

            ClosedStrip.enabled = true;
            ClosedCircle.enabled = true;
        }
    }
}
﻿using System;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class StateIndicatorController : MonoBehaviour
    {
        public StateIndicator State1; 
        public StateIndicator State2; 
        public StateIndicator State3; 
        public StateIndicator State4;

        [SerializeField] private float _flickDuration;
        [SerializeField] private int _flickCount;
        [SerializeField] private FlickState _flickState;

        public void ResetAll()
        {
            State1.Reset();
            State2.Reset();
            State3.Reset();
            State4.Reset();
        }
    
        private void Start()
        {
            ResetAll();
        }

        private void Update()
        {
            UpdateStateIndicator(State1);
            UpdateStateIndicator(State2);
            UpdateStateIndicator(State3);
            UpdateStateIndicator(State4);
        }

        public void UnlockNext()
        {
            if (State1.Unlocked == false)
            {
                State1.Unlocking = true;
                ResetFlick();
                return;
            }

            if (State2.Unlocked == false)
            {
                State2.Unlocking = true;
                ResetFlick();
                return;
            }

            if (State3.Unlocked == false)
            {
                State3.Unlocking = true;
                ResetFlick();
                return;
            }

            if (State4.Unlocked == false)
            {
                State4.Unlocking = true;
                ResetFlick();
                return;
            }
        }

        private void ResetFlick()
        {
            _flickDuration = 0.20f;
            _flickCount = 5;
            _flickState = FlickState.None;
        }

        private void UpdateStateIndicator(StateIndicator stateIndicator)
        {
            if (!stateIndicator.Unlocking) return;

            _flickState = UpdateFlick();
            switch (_flickState)
            {
                case FlickState.None:   return;

                case FlickState.Swap1:
                    stateIndicator.ClosedStrip.enabled = false;
                    stateIndicator.ClosedCircle.enabled = false;
                    stateIndicator.OpenStrip.enabled = true;
                    stateIndicator.OpenCircle.enabled = true;
                    break;

                case FlickState.Swap2:
                    stateIndicator.OpenStrip.enabled = false;
                    stateIndicator.OpenCircle.enabled = false;
                    stateIndicator.ClosedStrip.enabled = true;
                    stateIndicator.ClosedCircle.enabled = true;
                    break;

                case FlickState.Done:
                    stateIndicator.ClosedStrip.enabled = false;
                    stateIndicator.ClosedCircle.enabled = false;
                    stateIndicator.OpenStrip.enabled = true;
                    stateIndicator.OpenCircle.enabled = true;
                    stateIndicator.Unlocking = false;
                    stateIndicator.Unlocked = true;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private FlickState UpdateFlick()
        {
            if(_flickCount > 0 && _flickDuration > 0f)
            {
                _flickDuration -= Time.deltaTime;
            }

            if(_flickDuration <= 0f)
            {
                _flickDuration = 0.2f;
                _flickCount--;
            }

            if(_flickCount > 0)     return _flickState == FlickState.Swap1 ? FlickState.Swap2 : FlickState.Swap1;

            if(_flickCount == 0)    return FlickState.Done;

            return FlickState.None;
        }

        private enum FlickState
        {
            None,
            Swap1,
            Swap2,
            Done
        }
    }
}


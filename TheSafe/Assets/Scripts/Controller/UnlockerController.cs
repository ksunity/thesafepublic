﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Models;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controller
{
    public class UnlockerController : MonoBehaviour
    {
        [SerializeField] private Transform _unlockerTransform;
        [SerializeField] private Image _unlockerImage;
        [SerializeField] private float _speed;

        [SerializeField] private float _currentAngle;
        [SerializeField] private float _currentRange;
        [SerializeField] private Coverage _unlockerCoverage;

        [SerializeField] private bool _started;
        [SerializeField] private MotionType _motionType;
        [SerializeField] private float _fromAngle;
        [SerializeField] private float _toAngle;
        [SerializeField] private List<float> _jumpAngles;
        [SerializeField] private float _jumpDelayMax;
        [SerializeField] private float _jumpDelayCurrent;
        [SerializeField] private int _jumpIndex;

        //===== Go to left or right
        public void StartSimpleMotion(float startPosition, Range range, Speed speed, bool moveToLeft)
        {
            SetPosition(startPosition);

            var rangeDegree = (1f / 360f) * ((int)range);
            _currentRange = (float)range;
            SetRange(rangeDegree);

            var speedFloat = (float)speed;
            SetSpeedAndDirection(speedFloat, moveToLeft);

            _motionType = MotionType.Simple;

            _started = true;
        }

        //===== Go from X to Y and back
        public void StartFromToMotion(Range range, Speed speed, bool moveToLeft, float startPosition, float from, float to)
        {
            SetPosition(startPosition);

            var rangeDegree = (1f / 360f) * ((int)range);
            _currentRange = (float)range;
            SetRange(rangeDegree);

            var speedFloat = (float)speed;
            SetSpeedAndDirection(speedFloat, moveToLeft);

            _fromAngle = from;
            _toAngle = to;

            _motionType = MotionType.FromTo;

            _started = true;
        }

        //===== Jump between positions with some delay in between
        public void StartJumpMotion(Range range, Speed speed, params float[] positions)
        {
            _jumpAngles = new List<float>(positions);
            _jumpDelayMax = SpeedToDelay(speed);
            _jumpDelayCurrent = SpeedToDelay(speed);
            _jumpIndex = 0;

            var startPosition = _jumpAngles.FirstOrDefault();
            SetPosition(startPosition);

            var rangeDegree = (1f / 360f) * ((int)range);
            _currentRange = (float)range;
            SetRange(rangeDegree);

            _motionType = MotionType.Jump;

            _started = true;
        }

        public Coverage GetCoverage()
        {
            return _unlockerCoverage;
        }

        private void Start()
        {
            ResetAll();
        }

        public void ResetAll()
        {
            SetPosition(0f);
            SetRange(0f);
            SetSpeedAndDirection(0f, true);
            _started = false;

            _motionType = MotionType.None;
            _fromAngle = 0f;
            _toAngle = 0f;
            _jumpAngles = new List<float>();
            _jumpDelayMax = 0f;
            _jumpDelayCurrent = 0f;
            _jumpIndex = 0;
            _currentRange = 0f;
            _unlockerCoverage = new Coverage();
        }

        private void Update()
        {
            UpdateAngleAndCoverage();

            if (!_started) return;

            switch (_motionType)
            {
                case MotionType.None:
                    return;
                    
                case MotionType.Simple:
                    UpdatePosition();
                    break;

                case MotionType.FromTo:
                    UpdatePosition();
                    
                    if (_currentAngle >= _toAngle && _speed < 0f)
                    {
                        _speed *= -1;
                    }
                    else if (_currentAngle <= _fromAngle && _speed > 0f)
                    {
                        _speed *= -1;
                    }
                    break;

                case MotionType.Jump:
                    _jumpDelayCurrent -= Time.deltaTime;
                    if (_jumpDelayCurrent <= 0f)
                    {
                        _jumpIndex++;
                        if (_jumpIndex == (_jumpAngles.Count)) _jumpIndex = 0;
                        SetPosition(_jumpAngles[_jumpIndex]);
                        _jumpDelayCurrent = _jumpDelayMax;
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            UpdateAngleAndCoverage();
        }

        private void UpdateAngleAndCoverage()
        {
            _currentAngle = WrapEulerAngleFrom0To360(_unlockerTransform.eulerAngles.z);

            var from = _currentAngle;
            var to = _currentAngle + _currentRange;
            to = to > 360f ? WrapEulerAngleFrom0To360(to) : to;

            _unlockerCoverage.FromAngle = from;
            _unlockerCoverage.ToAngle = to;
        }

        private float WrapEulerAngleFrom0To360(float angle)
        {
            var wrapped = angle - 360f;
            wrapped = Math.Abs(wrapped);
            return wrapped;
        }
        
        private void UpdatePosition()
        {
            _unlockerTransform.Rotate(Vector3.forward * Time.deltaTime * _speed);
        }
        
        private void SetPosition(float rotation)
        {
            var q = _unlockerTransform.rotation;
            q.eulerAngles = new Vector3(0f, 0f, rotation);
            _unlockerTransform.rotation = q;
        }
        
        private void SetRange(float range)
        {
            _unlockerImage.fillAmount = range;
        }

        private void SetSpeedAndDirection(float speed, bool left)
        {
            _speed = left == false ? -1 * speed : speed;
        }

        private float SpeedToDelay(Speed speed)
        {
            switch (speed)
            {
                case Speed.SuperSlow:   return 1.00f;
                case Speed.Slow:        return 0.85f;
                case Speed.Medium:      return 0.70f;
                case Speed.Fast:        return 0.55f;
                case Speed.SuperFast:   return 0.40f;
                case Speed.InHuman:     return 0.25f;
                    
                default:                throw new ArgumentOutOfRangeException(nameof(speed), speed, null);
            }
        }

        private enum MotionType
        {
            None,
            Simple,
            FromTo,
            Jump
        }
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class CurrentNumberController : MonoBehaviour
    {
        [SerializeField] private Transform _currentNumber;

        public void SetPositionToDigit(int digit)
        {
            if(digit < 0 || digit > 9) return;

            if (digit == 0) SetPosition(0f);
            if (digit == 9) SetPosition(36f);
            if (digit == 8) SetPosition(72f);
            if (digit == 7) SetPosition(108f);
            if (digit == 6) SetPosition(144f);
            if (digit == 5) SetPosition(180f);
            if (digit == 4) SetPosition(216f);
            if (digit == 3) SetPosition(252f);
            if (digit == 2) SetPosition(288f);
            if (digit == 1) SetPosition(324f);
        }

        private void Start()
        {
            ResetAll();
        }

        public void ResetAll()
        {
            SetPosition(0f);
        }

        private void SetPosition(float rotation)
        {
            var q = _currentNumber.rotation;
            q.eulerAngles = new Vector3(0f, 0f, rotation);
            _currentNumber.rotation = q;
        }
    }
}

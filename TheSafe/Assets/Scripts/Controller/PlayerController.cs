﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controller
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private UnlockerController _unlockerController;
        [SerializeField] private CodeController _codeController;
        [SerializeField] private GameController _gameController;
        [SerializeField] private SoundController _soundController;

        [SerializeField] private Button _button;
        [SerializeField] private Button _switch;

        private void Start()
        {
            SetActive(false);
        }

        public void SetActive(bool value)
        {
            _button.enabled = value;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _codeController.NextStage();
            }
        }

        public void OnButtonAction()
        {
            var currentDigit = _codeController.GetCoverage();
            var unlockerCoverage = _unlockerController.GetCoverage();

            var currentFrom = currentDigit.FromAngle;
            var currentTo = currentDigit.ToAngle;
            
            var unlockerFrom = unlockerCoverage.FromAngle;
            var unlockerTo = unlockerCoverage.ToAngle;

            if (unlockerCoverage.ToAngle < unlockerCoverage.FromAngle)
            {
                unlockerTo = unlockerCoverage.ToAngle + 360f;
                currentTo = currentDigit.ToAngle < currentDigit.FromAngle ? currentDigit.ToAngle + 360f : currentDigit.ToAngle;
            }
            
            //Debug.Log($"currentFrom/To: [{currentFrom},{currentTo}], unlockerFrom/To: [{unlockerFrom},{unlockerTo}]");

            var currentLeft = currentFrom >= unlockerFrom && currentFrom <= unlockerTo;
            var currentRight = currentTo >= unlockerFrom && currentTo <= unlockerTo;

            var unlockerLeft = unlockerFrom >= currentFrom && unlockerFrom <= currentTo;
            var unlockerRight = unlockerTo >= currentFrom && unlockerTo <= currentTo;

            if (currentLeft || currentRight || unlockerLeft || unlockerRight)
            {
                //Debug.Log($"INSIDE! Current L = [{currentLeft}], Current R = [{currentRight}], Unlocker L = [{unlockerLeft}], Unlocker R = [{unlockerRight}]");
                _soundController.Unlock();
                _codeController.NextStage();
            }
            else
            {
                //Debug.Log("OUTSIDE !!!");
                _soundController.Wrong();
                _gameController.ResetStage();
            }
        }

        public void OnSwitch()
        {
            var fingerButtonPosition = _button.transform.position;
            _button.transform.position = _switch.transform.position;
            _switch.transform.position = fingerButtonPosition;
        }
    }
}

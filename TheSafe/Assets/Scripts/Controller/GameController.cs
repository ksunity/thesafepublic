﻿using System;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private IntroController _introController;
        [SerializeField] private CodeController _codeController;
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private StateIndicatorController _stateIndicatorController;
        [SerializeField] private UnlockerController _unlockerController;
        [SerializeField] private CodeDisplayController _codeDisplayController;
        [SerializeField] private CurrentNumberController _currentNumberController;
        [SerializeField] private WonUnlockedController _wonUnlockedController;

        [SerializeField] private GameState _gameState;

        private float _afterUnlockResetDelay = 8f;


        private void Start()
        {
            _gameState = GameState.Start;            
        }

        public void ResetStage()
        {
            _gameState = GameState.ResetStage;
        }

        public void Won()
        {
            _gameState = GameState.InitWin;
        }

        private void Update()
        {
            switch (_gameState)
            {
                case GameState.Start:
                    //Reset things here?
                    _gameState = GameState.IntroStart;
                    break;

                case GameState.IntroStart:
                    _introController.FadeOut();
                    _gameState = GameState.IntroFadingOut;
                    break;

                case GameState.IntroFadingOut:
                    if (_introController.IsDone()) _gameState = GameState.ResetStage;
                    break;

                case GameState.ResetStage:
                    _afterUnlockResetDelay = 8f;
                    _playerController.SetActive(false);
                    _stateIndicatorController.ResetAll();
                    _unlockerController.ResetAll();
                    _codeDisplayController.ResetAll();
                    _currentNumberController.ResetAll();
                    _codeController.ResetAll();
                    _wonUnlockedController.ResetAll();
                    _gameState = GameState.StartStage;
                    break;

                case GameState.StartStage:
                    _codeController.NextStage();
                    _playerController.SetActive(true);
                    _gameState = GameState.Idle;
                    break;

                case GameState.Idle:
                    //Wait for player input
                    break;

                case GameState.InitWin:
                    _playerController.SetActive(false);
                    _wonUnlockedController.FadeIn();
                    _gameState = GameState.WinDone;
                    break;

                case GameState.WinDone:
                    if (_wonUnlockedController.IsDone())
                        _gameState = GameState.Completed;
                    break;
                    
                case GameState.Completed:
                    _afterUnlockResetDelay -= Time.deltaTime;
                    if (_afterUnlockResetDelay <= 0f)
                        _gameState = GameState.ResetStage;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private enum GameState
        {
            Start,
            IntroStart,
            IntroFadingOut,
            ResetStage,
            StartStage,
            Idle,
            InitWin,
            WinDone,
            Completed,
        }
    }
}

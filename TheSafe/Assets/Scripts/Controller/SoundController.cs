﻿using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class SoundController : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        [SerializeField] private AudioClip _unlock1;
        [SerializeField] private AudioClip _unlock2;
        [SerializeField] private AudioClip _wrong;
        [SerializeField] private AudioClip _nextStage;
        [SerializeField] private AudioClip _win;

        public void Unlock()
        {
            _audioSource.PlayOneShot(Random.Range(0f, 1f) >= 0.5f ? _unlock1 : _unlock2);
        }

        public void Wrong()
        {
            _audioSource.PlayOneShot(_wrong);
        }

        public void NextStage()
        {
            _audioSource.PlayOneShot(_nextStage);
        }

        public void Win()
        {
            _audioSource.PlayOneShot(_win);
        }
    }
}

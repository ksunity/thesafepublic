﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class CodeDisplayController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _num1;
        [SerializeField] private TextMeshProUGUI _num2;
        [SerializeField] private TextMeshProUGUI _num3;
        [SerializeField] private TextMeshProUGUI _num4;
        [SerializeField] private TextMeshProUGUI _num5;
        
        private readonly Color _notCurrentColor = new Color(0.5647059f, 0.7607844f, 0.8000001f, 1.0f);
        private readonly VertexGradient _notCurrentColorGradient = new VertexGradient(
            new Color(0.325f, 0.592f, 0.592f, 1.000f),
            new Color(0.596f, 0.769f, 0.812f, 1.000f),
            new Color(0.596f, 0.769f, 0.812f, 1.000f),
            new Color(0.325f, 0.592f, 0.592f, 1.000f));

        private readonly Color _currentColor = new Color(0.6784314f, 0.8196079f, 0.9058824f, 1.0f);
        private readonly VertexGradient _currentColorGradient = new VertexGradient(
            new Color(1f, 1f, 1f, 1.000f),
            new Color(1f, 1f, 1f, 1.000f),
            new Color(1f, 1f, 1f, 1.000f),
            new Color(1f, 1f, 1f, 1.000f));

        private int _currentDigitIndex = 0;
        
        public void SetCurrentDigit(int value)
        {
            NextDigit();

            SetPrevToNotCurrent();

            if(_currentDigitIndex == 1) SetNumber(_num1, value.ToString(), true);
            if(_currentDigitIndex == 2) SetNumber(_num2, value.ToString(), true);
            if(_currentDigitIndex == 3) SetNumber(_num3, value.ToString(), true);
            if(_currentDigitIndex == 4) SetNumber(_num4, value.ToString(), true);
            if(_currentDigitIndex == 5) SetNumber(_num5, value.ToString(), true);
        }

        private void SetPrevToNotCurrent()
        {
            if(_currentDigitIndex == 1)
            {
                SetNumber(_num2, "_", false);
                SetNumber(_num3, "_", false);
                SetNumber(_num4, "_", false);
                SetNumber(_num5, "_", false);
            }

            if (_currentDigitIndex == 2)    SetNumber(_num1, null, false);
            if (_currentDigitIndex == 3)    SetNumber(_num2, null, false);
            if (_currentDigitIndex == 4)    SetNumber(_num3, null, false);
            if (_currentDigitIndex == 5)    SetNumber(_num4, null, false);
        }

        private void NextDigit()
        {
            _currentDigitIndex++;
            if (_currentDigitIndex == 6) _currentDigitIndex = 1;
        }

        private void Start()
        {
            ResetAll();
        }

        public void ResetAll()
        {
            SetNumber(_num1, "_", false);
            SetNumber(_num2, "_", false);
            SetNumber(_num3, "_", false);
            SetNumber(_num4, "_", false);
            SetNumber(_num5, "_", false);

            _currentDigitIndex = 0;
        }
        
        private void SetNumber(TMP_Text text, string value, bool isCurrent)
        {
            if(!string.IsNullOrWhiteSpace(value)) text.text = value;
            text.color = isCurrent ? _currentColor : _notCurrentColor;
            text.colorGradient = isCurrent ? _currentColorGradient : _notCurrentColorGradient;
        }
    }
}

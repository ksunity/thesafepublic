﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controller
{
    public class IntroController : MonoBehaviour
    {
        [SerializeField] private Image _panel;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _byMe;

        private bool _start;
        private bool _done;
        private float _currentAlpha;

        public void FadeOut()
        {
            _start = true;
            _done = false;
        }

        public bool IsDone()
        {
            return _done;
        }

        private void Start()
        {
            ResetAll();
        }

        private void ResetAll()
        {
            _currentAlpha = 1f;

            _panel.enabled = true;
            _title.enabled = true;
            _byMe.enabled = true;

            SetPanelAlpha(_currentAlpha);
            SetTextAlpha(_currentAlpha);
            _start = false;
            _done = false;
        }

        private void SetPanelAlpha(float value)
        {
            var panelColor = _panel.color;
            var newPanelColor = new Color(panelColor.r, panelColor.g, panelColor.b, value);
            _panel.color = newPanelColor;
        }

        private void SetTextAlpha(float value)
        {
            var titleColor = _title.color;
            var newTitleColor = new Color(titleColor.r, titleColor.g, titleColor.b, value);
            _title.color = newTitleColor;

            var byMeColor = _byMe.color;
            var newByMeColor = new Color(byMeColor.r, byMeColor.g, byMeColor.b, value);
            _byMe.color = newByMeColor;
        }

        private void Update()
        {
            if(!_start) return;
            _currentAlpha -= 0.25f * Time.deltaTime;

            if (_currentAlpha <= 0f) _currentAlpha = 0f;
            SetPanelAlpha(_currentAlpha);
            SetTextAlpha(_currentAlpha);

            if (_currentAlpha <= 0f)
            {
                _done = true;
                _start = false;
                _panel.enabled = false;
                _title.enabled = false;
                _byMe.enabled = false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class CodeController : MonoBehaviour
    {
        [SerializeField] private CodeDisplayController _codeDisplayController;
        [SerializeField] private CurrentNumberController _currentNumberController;
        [SerializeField] private UnlockerController _unlockerController;
        [SerializeField] private StateIndicatorController _stateIndicatorController;
        [SerializeField] private GameController _gameController;
        [SerializeField] private SoundController _soundController;

        [SerializeField] private int _sequence;
        [SerializeField] private int _stage;
        [SerializeField] private Coverage _currentDigitCoverage;

        //This could be loaded from JSON
        //new CodePattern(PatternType.Simple, 5, Range.Normal, Speed.Slow, false, 270f, null),
        private readonly Dictionary<int, List<CodePattern>> _stageSequence = new Dictionary<int, List<CodePattern>>()
        {
            {
                /*=== STAGE ===*/ 1,
                new List<CodePattern>()
                {
                    new CodePattern(PatternType.Simple, 3, Range.Normal, Speed.SuperSlow, false, 0f, null),
                    new CodePattern(PatternType.FromTo, 7, Range.Small, Speed.Slow, false, 209f, 137f, 317f),
                    new CodePattern(PatternType.Jump, 4, Range.Tiny, Speed.Medium, false, 0f, 108f+4.5f, 324f+4.5f, 72f+4.5f, 216f+4.5f, 252f+4.5f),
                    new CodePattern(PatternType.Simple, 1, Range.Tiny, Speed.Medium, true, 0f, null),
                    new CodePattern(PatternType.Jump, 8, Range.Normal, Speed.SuperFast, false, 0f, 72f+18f, 252f+18f, 108f+18f, 288f+18f),
                }
            },
            {
                /*=== STAGE ===*/ 2,
                new List<CodePattern>()
                {
                    new CodePattern(PatternType.FromTo, 3, Range.Small, Speed.Fast, false, 288f+9f, 72f+9f, 180f+9f),
                    new CodePattern(PatternType.Simple, 7, Range.Normal, Speed.Fast, true, 90f, null),
                    new CodePattern(PatternType.Simple, 2, Range.Wide, Speed.Fast, false, 90f, null),
                    new CodePattern(PatternType.Jump, 5, Range.Normal, Speed.SuperFast, false, 0f, 180f+18f, 36f+18f, 108f+18f, 288f+18f, 216f+18f),
                    new CodePattern(PatternType.Simple, 0, Range.Normal, Speed.Fast, true, 180f, null),
                }
            },
            {
                /*=== STAGE ===*/ 3,
                new List<CodePattern>()
                {
                    new CodePattern(PatternType.Jump, 8, Range.Tiny, Speed.InHuman, false, 0f, 288f+4.5f, 72f+4.5f),
                    new CodePattern(PatternType.Simple, 5, Range.Normal, Speed.Fast, false, 111f, null),
                    new CodePattern(PatternType.FromTo, 5, Range.Small, Speed.Fast, true, 216f+9f, 144f+9f, 324f+9f),
                    new CodePattern(PatternType.Simple, 3, Range.VeryWide, Speed.SuperFast, true, 90f, null),
                    new CodePattern(PatternType.Jump, 3, Range.Tiny, Speed.InHuman, false, 0f, 252f+4.5f, 252-10f, 252f+18f),
                }
            },
            {
                /*=== STAGE ===*/ 4,
                new List<CodePattern>()
                {
                    new CodePattern(PatternType.Jump, 7, Range.Tiny, Speed.Fast, false, 0f, 108f+4.5f, 324f+4.5f, 72f+4.5f, 216f+4.5f, 252f+4.5f),
                    new CodePattern(PatternType.Jump, 4, Range.Normal, Speed.SuperFast, false, 0f, 180f+18f, 36f+18f, 108f+18f, 288f+18f, 216f+18f),
                    new CodePattern(PatternType.Simple, 8, Range.Normal, Speed.Fast, true, 90f, null),
                    new CodePattern(PatternType.FromTo, 4, Range.Small, Speed.Fast, false, 288f+9f, 72f+9f, 180f+9f),
                    new CodePattern(PatternType.Simple, 5, Range.Normal, Speed.InHuman, false, 0f, null),
                }
            }
        };

        public Coverage GetCoverage()
        {
            return _currentDigitCoverage;
        }

        private void Start()
        {
            ResetAll();
        }

        public void ResetAll()
        {
            _sequence = 0;
            _stage = 1;
            _currentDigitCoverage = new Coverage();
        }
        
        public void NextStage()
        {
            _sequence++;
            if (_sequence == 6)
            {
                _sequence = 1;
                _stage++;

                //===== WON ===== UNLOCKED THE SAFE =====
                if (_stage == 5) //Finished 4 stages so unlocked the safe
                {
                    _soundController.Win();
                    _stateIndicatorController.UnlockNext();
                    _gameController.Won();
                    return;
                }
                //======================================

                _soundController.NextStage();
                _stateIndicatorController.UnlockNext();
            }

            var cp = _stageSequence[_stage][_sequence-1];
            switch (cp.Type)
            {
                case PatternType.Simple:
                    SetCurrentDigit(cp.Digit);
                    _unlockerController.StartSimpleMotion(cp.StartPosition, cp.Range, cp.Speed, cp.MoveToLeft);
                    break;

                case PatternType.FromTo:
                    SetCurrentDigit(cp.Digit);
                    _unlockerController.StartFromToMotion(cp.Range, cp.Speed, cp.MoveToLeft, cp.StartPosition, 
                        cp.PositionParams.First(), cp.PositionParams.Skip(1).First());
                    break;

                case PatternType.Jump:
                    SetCurrentDigit(cp.Digit);
                    _unlockerController.StartJumpMotion(cp.Range, cp.Speed, cp.PositionParams);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private void SetCurrentDigit(int digit)
        {
            _currentDigitCoverage = SetCurrentCoverage(digit);
            _codeDisplayController.SetCurrentDigit(digit);
            _currentNumberController.SetPositionToDigit(digit);
        }

        private static Coverage SetCurrentCoverage(int digit)
        {
            if (digit == 0) return new Coverage() { FromAngle = 353f, ToAngle = 7f };
            if (digit == 1) return new Coverage() { FromAngle = 29f, ToAngle = 43f };
            if (digit == 2) return new Coverage() { FromAngle = 65f, ToAngle = 79f };
            if (digit == 3) return new Coverage() { FromAngle = 101f, ToAngle = 115f };
            if (digit == 4) return new Coverage() { FromAngle = 137f, ToAngle = 151f };
            if (digit == 5) return new Coverage() { FromAngle = 173f, ToAngle = 187f };
            if (digit == 6) return new Coverage() { FromAngle = 209f, ToAngle = 223f };
            if (digit == 7) return new Coverage() { FromAngle = 245f, ToAngle = 259f };
            if (digit == 8) return new Coverage() { FromAngle = 281f, ToAngle = 295f };
            if (digit == 9) return new Coverage() { FromAngle = 317f, ToAngle = 331f };

            throw new ArgumentException($"Digit value is incorrect: [{digit}]");
        }

        //DEBUG
        //private int _digit = 0;
        //private void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.Space))
        //    {
        //        _digit++;
        //        if (_digit == 10) _digit = 0;
        //        SetCurrentDigit(_digit);
        //    }
        //}
    }
}
